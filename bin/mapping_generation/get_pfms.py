import argparse
import pandas as pd
import os

def parse_args():
    '''
    Parse passed arguments.
    '''
    parser = argparse.ArgumentParser(
        description = 'Find the relevant PFMs for a dataset and create a mapping.')
    
    parser.add_argument(
        '--tf_mapping',
        help = 'Path to the TF mapping file.',
        type = str,
        required = True)

    parser.add_argument(
        '--pfm_folder',
        help = 'Path to the folder containing the PFMs.',
        type = str,
        required = True)

    parser.add_argument(
        '--dataset_id',
        help = 'Dataset ID for the file being processed.',
        type = str,
        required = True)

    parser.add_argument(
        '--tf_name',
        help = 'Name of the TF being processed.',
        type = str,
        required = True)

    parser.add_argument(
        '--output',
        help = 'Path to the output file.',
        type = str,
        required = True)
    
    args = parser.parse_args()

    return args


def main(
        tf_mapping: str,
        pfm_folder: str,
        dataset_id: str,
        tf_name: str,
        output: str) -> None:
    '''
    '''
    mapping = pd.read_csv(tf_mapping, header = 0, sep = "\t")

    pfm_rows = mapping.loc[
        (mapping.DATA_SET == dataset_id) & (mapping.TF_NAME == tf_name), ]

    # Remove trailing / from pfm_folder if it's there
    pfm_folder = pfm_folder.rstrip('/')

    matrix_ids = [
        f"{pfm_folder}/{row.PFM_ID}.{row.VERSION}.jaspar" for index, row in pfm_rows.iterrows()]

    if len(matrix_ids) == 0:
        raise AssertionError(f"No PFMs found for {dataset_id} - {tf_name}.")

    output_mapping = open(output, 'w')
    output_mapping_content = '\n'.join(matrix_ids)
    output_mapping.write(output_mapping_content)
    output_mapping.close()


if __name__ == "__main__":
    args = parse_args()
    main(
        tf_mapping = args.tf_mapping,
        pfm_folder = args.pfm_folder,
        dataset_id = args.dataset_id,
        tf_name = args.tf_name,
        output = args.output)

