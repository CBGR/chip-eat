#!/bin/bash

source "bin/logme.sh"

dataset_id=$1;
tf_name=$2;
pfm_folder=$3;
TF_map=$4;
out_dir=$5;
output=$6;

if [ ! -e $TF_map ]
then
  echo "No TF to PFM mapping file found. Exiting.."
  exit 1
elif [ ! -e ${TF_map}.sorted ]
then
  sort -t $'\t' -k 4 $TF_map > ${TF_map}.sorted
fi
tf_pfm_map=${TF_map}.sorted


noPFM=true;
pfm_id=;
pfm_version=;

while IFS=$'\t' read -r -a line
  do
    data_set_map="${line[3]}"
    data_set_map="$(echo "$data_set_map" | tr '[:lower:]' '[:upper:]')"

    # speed up the file parsing a bit
    if [[ "$data_set_map" < "$dataset_id" ]]
    then
      continue
    elif [[ "$data_set_map" == "$dataset_id" ]]
    then
      data_set_tf_name="${line[0]}"
      pfm_id="${line[1]}"
      pfm_version="${line[2]}"

      #to upper case
      data_set_tf_name="$(echo "$data_set_tf_name" | tr '[:lower:]' '[:upper:]')"
    elif [[ "$data_set_map" > "$dataset_id" ]]
    then
      break
    fi

    # check if we got a match on data set, cell line and TF
    if [ "$dataset_id" == "$data_set_map" ] && [ "$tf_name" == "$data_set_tf_name" ]
    then
      #is there a PFM for this TF
      if [ "$pfm_id" == "NA" ]
      then
        logme "No PFM for the transcription factor $data_set_tf_name in data set $dataset_id was found. Skipping.."
        #rm -R $out_dir
        exit 0
      # remove parent directory also if empty
      if [ -z "$(ls -A "$(dirname $out_dir)")" ]
      then
          rm -R "$(dirname $out_dir)"
      fi
      break
      else
          pfms+=($pfm_folder/${pfm_id}.${pfm_version})
          noPFM=false
      fi
    else
      logme "Something does not match. Make sure the mapping file is correct!"
      logme "Dataset from file name: $dataset_id"
      logme "Dataset from map: $data_set_map"
      logme "TF from file name: $tf_name"
      logme "TF from map: $data_set_tf_name"
      logme "---------------------------------------"
    fi
done < $tf_pfm_map

for pfm in ${pfms[@]}
do
  echo ${pfm}.jaspar >> $output
done

