
# 1. Read arguments

message("Reading arguments")

args = commandArgs(trailingOnly = T)

peakFilePath = args[1]
resultFilePath = args[2]
outputFilePath = args[3]

# 2. Open peaks file

message("Reading Peaks file")

peakFile = read.delim(peakFilePath, sep = "\t", header = F, stringsAsFactors = F)
colnames(peakFile) = c("chr", "start", "end", "Peak_ID", "Score", "Strand", "Fold_change_at_summit", "log10pval_summit", "log10qval_summit", "Dist_to_summit")

# 3. Open results file

message("Reading results file")

resultFile = read.delim(resultFilePath, sep = "\t", header = T, stringsAsFactors = F)

# 4. Subset peaks with TFBS in the EZ and center them at the peak summit

peaks_with_TFBS = peakFile[peakFile$Peak_ID %in% resultFile$peak_ID, ]
peaks_with_TFBS$end = peaks_with_TFBS$start + peaks_with_TFBS$Dist_to_summit + 1
peaks_with_TFBS$start = peaks_with_TFBS$start + peaks_with_TFBS$Dist_to_summit

# 5. Export

message("Writing output table")

write.table(peaks_with_TFBS[, 1:6], outputFilePath, sep = "\t", col.names = F, row.names = F, quote = F)

message("Done! :D")
