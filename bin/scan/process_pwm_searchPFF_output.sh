input_file=$1
output_file=$2

awk -v OFS='\t' -F"::" '$1=$1' $input_file \
| awk -v OFS='\t' '{print $2, $3, $4, $5, $6, $7, $8, $9, $10, $1}' \
| awk -v OFS='\t' -F":" '$1=$1' \
| awk -v OFS='\t' '{gsub(/\-/, "\t", $2)} 1' \
| awk -v OFS='\t' '{print $1, $2, $3, $6, $11, $5, (($10 - $9) + 1), (($9 + $10) / 2) - '500', $8, $7, $9, $10, $12}' \
> $output_file