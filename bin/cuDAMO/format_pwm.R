# =============== #
# Library loading #
# =============== #

required.libraries = c("optparse")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only=TRUE, quietly = T))
}

# ============== #
# Read arguments #
# ============== #

option_list = list(

  make_option(c("-i", "--input_pwm"), type="character", default = NULL,
              help="Path to the input PWM (Mandatory)", metavar="character")

);
opt_parser = OptionParser(option_list = option_list);
opt = parse_args(opt_parser);

input_pwm = opt$input_pwm

# ============== #
# Read input PWM #
# ============== #

pwm = read.delim(input_pwm, header = F)

# ============= #
# Transpose pwm #
# ============= #

pwm_t = t(pwm)

# ============== #
# Save to output #
# ============== #

write.table(pwm_t, input_pwm, col.names = F, row.names = F, quote = F, sep = "\t")