DIR=$( realpath "$( dirname "$0" )" )

# ===== #
# Usage #
# ===== #

usage() {
    echo "Usage: $0 -m MAPPING -f FOREGROUND 50 FASTA -b BACKGROUND 50 FASTA -s FOREGROUND 500 FASTA -o OUTPUT DIR"
      echo ""
      echo "Run chip-eat pipeline with given data and peaks directories."
      echo ""
      echo "Arguments:"
      echo ""
      echo "    -m MAPPING: path to a file mapping to all PFMs to use."
      echo "    -f FOREGROUND 50 FASTA: path to the 50 bp foreground fasta file."
      echo "    -b BACKGROUND 50 FASTA: path to the 50 bp background fasta file."
      echo "    -s FOREGROUND 500 FASTA: path to the 500 bp foreground fasta file."
      echo "    -o OUTPUT DIR: path to the output directory."
      echo ""
      exit 0
}

# ================ #
# Argument parsing #
# ================ #

echo "Parsing arguments..."

while getopts "m:f:b:s:o:h" options; do
  case "${options}" in
    m) mapping=${OPTARG} ;;
    f) foreground_50=${OPTARG} ;;
    b) background=${OPTARG} ;;
    s) foreground_500=${OPTARG} ;;
    o) output_dir=${OPTARG} ;;
    h) usage ;;
  esac
done

mkdir -p $output_dir

for seed in $(cat $mapping)
do

  pfm_id=$(basename ${seed%".jaspar"})

  cudamo \
    -o ${output_dir}/${pfm_id}.damo \
    $foreground_50 \
    $background \
    $seed

  if [ -e ${output_dir}/${pfm_id}.damo ]
  then

    sed -e 's/^ *//; s/  */\t/g' \
        ${output_dir}/${pfm_id}.damo \
        > ${output_dir}/${pfm_id}.damo.tmp

    mv \
        ${output_dir}/${pfm_id}.damo.tmp \
        ${output_dir}/${pfm_id}.damo

    Rscript ${DIR}/../cuDAMO/format_pwm.R \
        -i ${output_dir}/${pfm_id}.damo

  else

    echo "Processing failed in matrix optimization with cuDAMO." \
    > $(dirname ${output_dir})/log.txt
    rm -r ${output_dir}/${pfm_id}*
    exit 0

  fi

done

successful_files=$(find $output_dir -name "*.damo")

for file in ${successful_files[@]}
do

  dset=$(basename $(dirname $(dirname $file)))
  dset_parts=(${dset//_/ })
  tf_name=${dset_parts[-1]}

  ${DIR}/../scan/pwm_searchPFF \
    $file \
    $foreground_500 \
    0.0 \
    -b \
    -n $tf_name \
    > ${file%".damo"}_norescan

  Rscript ${DIR}/../scan/reverse_complement_negative_strands.R \
    ${file%".damo"}_norescan

  bash ${DIR}/../scan/process_pwm_searchPFF_output.sh \
    ${file%".damo"}_norescan \
    ${file%".damo"}_norescan.score

  status=$(Rscript ${DIR}/../enrichment_zone/call_thresholds.R \
    ${DIR}/../enrichment_zone/compute_thresholds.R \
    ${DIR}/../../ \
    ${DIR}/../enrichment_zone/ \
    ${file%".damo"}_norescan.score \
    "relative" \
    7 \
    8 \
    9 \
    500 \
    ${file%".damo"}_norescan.score.ez)

  if [ $status == "NOK" ]
  then

    echo "Processing failed in enrichment zone computation for matrix $(basename ${file%".damo"})." \
    >> $(dirname ${output_dir})/log.txt
    rm -r ${output_dir}/${pfm_id}*
    exit 0

  fi  

done