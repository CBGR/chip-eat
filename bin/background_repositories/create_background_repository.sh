#!/bin/bash

# This script will take as input a FASTA file containing the whole genome of an
# organism and create a background repository using BiasAway. The steps are:

# 1. Split the genome in sequences of N bp (where N is a parameter specified by the user).
# 2. Remove sequences containing Ns
# 3. Generate the background sequences

# The uppercase variables are defined in the config file. They contain the
# paths to the necessary files to generate the background sequences.

# Arguments:

# 1. Input FASTA sequence
# 2. Size of background sequences
# 3. Output directory

ProgName=$(basename $0);
DirName=$(dirname $0);
  
sub_help(){
    echo -e "\nUsage: $ProgName -f <input fasta> -s <bin size> -r <output repository>\n";
    echo -n "-f <input fasta>: Fasta file containing the genome sequence in ";
    echo "FASTA format";
    echo "-s <bin size>: size of the bins to split the genome sequence";
    echo -n "-r <output rep>: output repository name to be created, which ";
    echo -e "will contain the files useful for BiasAway\n";
}
 

TEMP=`getopt -o f:s:r:h -- "$@"`;
if [ $? != 0 ]
then
  echo "Terminating..." >&2;
  exit 1;
fi
eval set -- "$TEMP";

binsize=1000
while true
do
  case "$1" in
    -f) fastafile=$2; shift 2;;
    -s) binsize=$2; shift 2;;
    -r) outdir=$2; shift 2;;
    -h) sub_help; exit;;
    --) shift; break;;
    *) echo "Internal error!"; exit 1;;
  esac
done

# Checking options and parameters
if [ ! -e $fastafile ]
then
    echo -e "FASTA file $fastafile does not exist\n";
    exit 1;
fi
#if [ -d $outdir ]
#then
#    echo -e "Output directory $outdir already exists\n";
#    exit 1;
#else
#    mkdir -p $outdir;
#fi;

mkdir -p $outdir

# 1. Split the genome in kmers of N size

echo "Splitting the genome into defined kmers"

tmpdir=$(mktemp -d);
cp $fastafile $tmpdir/;
fastaname=$(basename $fastafile);
pyfasta split -n 1 -k $binsize $tmpdir/$fastaname;

# Clean up intermediate files
rm $tmpdir/$fastaname.gdx $tmpdir/$fastaname.flat

if [ $(expr ${binsize} % 1000) -eq 0 ]
then
  km_filename=$(expr ${binsize} / 1000)
  split_fasta=${fastaname%.*}.split.${km_filename}Kmer.fa

else
  split_fasta=${fastaname%.*}.split.${binsize}mer.fa
fi

split_clean_fasta=$(basename $split_fasta .fa)_clean.fa

# 2. Remove sequences different than binsize

awk -v RS=">" -v FS="\n" '{for(i=2;i<NF;i++) {l=length($i)}; if(l=='${binsize}') printf ">%s", $0}' $tmpdir/$split_fasta > $tmpdir/$split_clean_fasta

# 3. Generate the background sequences

echo "Generating background repository with BiasAway"

tmp=$(mktemp -p $tmpdir);
biasaway g -b $tmpdir/$split_clean_fasta -r $outdir -f $tmp;
rm -r $tmpdir;
