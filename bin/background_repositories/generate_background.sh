DIR=$( realpath "$( dirname "$0" )" )

# ===== #
# Usage #
# ===== #

usage() {
    echo "Usage: $0 -p PEAK FILE -g GENOME FASTA -c CHROM SIZES -r BACKGROUND REPOSITORY -e EXTENSION SIZE -o OUTPUT DIR"
      echo ""
      echo "Generate a background fasta file."
      echo ""
      echo "Arguments:"
      echo ""
      echo "    -p PEAK FILE: path to a narrowPeak file with peaks centered at the summit."
      echo "    -g GENOME FASTA: path to a genomic fasta file."
      echo "    -c CHROM SIZES: path to a chromosome sizes file."
      echo "    -r BACKGROUND REPOSITORY: path to a background repository generated with BiasAway."
      echo "    -e EXTENSION SIZE: number of nucleotides to extend at each side of the peak summit."
      echo "    -o OUTPUT DIR: path to the output directory. The directory will be created if it does not exist."
      echo ""
      exit 0
}

# ================ #
# Argument parsing #
# ================ #

while getopts "p:g:c:r:e:o:h" options; do
  case "${options}" in
    p)
      peak_file=${OPTARG}
      ;;
    g)
      genome=${OPTARG}
      ;;
    c)
      chrom_sizes=${OPTARG}
      ;;
    r)
      background_repository=${OPTARG}
      ;;
    e)
      extension_size=${OPTARG}
      ;;
    o)
      output_dir=${OPTARG}
      if [ ! -e $output_dir ]
      then
        mkdir -p $output_dir
      fi
      ;;
    h)
      usage
      ;;
  esac
done

# ==== #
# Main #
# ==== #

foreground_bed=${output_dir}/${extension_size}bp.foreground
foreground_fasta=${output_dir}/${extension_size}bp.foreground.fa
background_fasta=${output_dir}/${extension_size}bp.background.fa

# ================================= #
# Extend regions by provided amount #
# ================================= #

bedtools slop \
    -i $peak_file \
    -g $chrom_sizes \
    -b $extension_size \
    > $foreground_bed

# ================== #
# Format slop output #
# ================== #

awk -v OFS='\t' \
    -f ${DIR}/format_slop_output.awk \
    $foreground_bed \
    > ${foreground_bed}.tmp

mv \
    ${foreground_bed}.tmp \
    $foreground_bed

# ================== #
# Get fasta sequence #
# ================== #

bedtools getfasta \
    -fi $genome \
    -bed $foreground_bed \
    -name \
    > $foreground_fasta

# ================================= #
# Generate background with BiasAway #
# ================================= #

biasaway g \
    -r $background_repository \
    -f $foreground_fasta \
    > $background_fasta