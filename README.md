# ChIP-eat

The chip-eat pipeline processes called ChIP-seq peaks to obtain accurate, high-confidence transcription factor binding site (TFBS) predictions. Briefly, the pipeline takes as input the called peaks and a set of JASPAR PFMs for the TF used in the ChIP-seq experiment. As a first step, the pipeline optimizes the PWMs in a dataset-specific manner. Next, it takes the optimized PWM and scans each peak with it, recording the position of the best scoring subsequence per peak and its distance to the peak summit. Next, a non-parametric, data driven, entropy-based thresholding algorithm is applied to automatically define an enrichment zone within which we find the highest confidence predicted TFBSs. Finally, it rescans all peaks with the optimized PWM to obtain the best TFBS per peak falling withinin the enrichment zone.

The chip-eat pipeline is written in Snakemake. Additionally, a container is available at [docker hub](https://hub.docker.com/r/firriver/chip-eat).

### Prerequisites

The following software is required to run the pipeline:

- Python 3.10.4
- Snakemake >= 7.26.0
- Apptainer

The rest of dependencies are available in the container at our docker hub.

### Installation of the pipeline

Clone this repository by running:
```
git clone git@bitbucket.org:CBGR/chip-eat.git
```

The repository includes the source code for the pipeline, the code to set up the container, and the Snakemake pipeline.  

### Preparation of the data for the ChIP-eat pipeline

The chip-eat pipeline requires the following two inputs:

* Path to `peaks` directory: a path to a directory containing the narrowPeak files with the called peaks per dataset.
* Path to `data` directory: a path to a directory containing:
    + A set of JASPAR PFMs in JASPAR format.
    + A set of genome fasta files.
    + A set of genome chromosome sizes files.
    + A dataset to JASPAR PFM mapping.

Please see below for a full description of the expected structure and content of these two directories.

#### Peaks directory preparation

The pipeline expects the `peaks` directory to show a specific structure and file naming to automatically process the data. The expected structure is as follows:
```
peaks/
└── {assembly} (e.g. hg38)
    └── {datasetID}_{metadata}_{TFname} (e.g. EXP057964_MCF10A-breast-epithelial-cells-_TP53)
        └── {datasetID}_{metadata}_{TFname}_peaks.narrowPeak (EXP057964_MCF10A--breast-epithelial-cells-_TP53_peaks.narrowPeak)

```

Here, all datasets are structured by genome assembly, while `{datasetID}` is a unique identifier for each dataset, `{metadata}` contains some information about the experiment (cell line, condition, etc), and `{TFname}` is the name of the TF studied in the ChIP-seq experiment. The pipeline expects to find inside each dataset directory a `.narrowPeak` file with the following naming:

`datasetID_metadata_TFname_peaks.narrowPeak`

**Important!** please note that `TFname` and `datasetID` from both the dataset directory and the `.narrowPeak` file must coincide respectively with the `TF_NAME` and `DATA_SET` fields in the TF mapping (see below for more information).

#### Data directory preparation

The data directory is expected to be structured as follows:
```
data/
├── chrom_sizes
│   └── {assembly}.chrom.sizes (e.g. hg38.chrom.sizes)
├── genomes
│   └── {assembly}.fa (e.g. hg38.fa)
├── JASPAR
│   └── {matrix_ID}.{matrix_version}.jaspar (e.g. MA0007.3.jaspar)
└── TF_mapping
    └── TF_mapping_merged.tsv

```

##### Chromosome sizes and genome fasta files

These files contain the size and nucleotide sequence of each chromosome for a specific genome assembly. They can be downloaded from UCSC, for example. The `chrom.sizes` and `fasta` files are expected to be under directories named `chrom_sizes` and `genomes`, respectively. 

##### JASPAR matrices

The `JASPAR` directory contains collection of all JASPAR-formatted PFMs. These files can be easily obtained from the [JASPAR downloads page](https://jaspar.genereg.net/downloads/). For example, the set of JASPAR-formatted non-redundant PFMs from the CORE collection in the JASPAR 2022 release can be downloaded from https://jaspar.genereg.net/download/data/2022/CORE/JASPAR2022_CORE_non-redundant_pfms_jaspar.zip. The image below shows where this is found within the [JASPAR downloads page](https://jaspar.genereg.net/downloads/):

![Downloading the JASPAR-formatted CORE PFMs](img/JASPAR.png)

The required files can then be obtained by simply extracting the content of the compressed file within the `JASPAR` directory.

##### TF mapping

The ChIP-eat pipeline requires a mapping table that indicates which JASPAR PFMs should be used to scan each dataset. The table is a .tsv file with four different fields named as follows:

- TF_NAME: the name of the TF.
- PFM_ID: the identifier of the PFM.
- VERSION: the PFM version.
- DATA_SET: the dataset identifier. 

See below for an example of a TF mapping table:

| TF_NAME | PFM_ID | VERSION | DATA_SET |
|---|---|---|---|
| ESRRA | MA0592 | 2 | ENCSR473SUA |

### Configuring the ChIP-eat pipeline

Before running, the `config.yaml` file will need to be configured. This file is found in the same directory as the Snakefile. In the config file, you will need to set the paths to different directories containing the input data, the output directory, the URL of the container version to use, and some slurm-specific parameters.

**Note!** When running the pipeline using singularity, make sure all paths in the configuration file are relative to the Snakefile!

### Running the ChIP-eat pipeline

Once configured, the pipeline can be run in one simple command once the required directories are provided. To do so, navigate to the path where the Snakemake file is and run:
```
snakemake \
    -p \
    --cores CORES \
    --use-singularity \
    --singularity-args '\-e'
```

where `CORES` indicates the number of cores to use. 

## Example datasets

We provide a small collection of example datasets in the `example_files` directory. There, the users can find the genomic files, some ChIP-seq datasets, the TF mapping for them, and the config.yaml file. To run it, follow these steps:

1. Decompress the file in `example_inputs.tar.gz` with the following command, which will create the expected directory structures:
```
tar zxvf example_inputs.tar.gz
```
2. Move the directories from the decompressed file into the data directory.
```
mv example_files/* data/
```
3. Run the pipeline with:
```
snakemake -p --cores 2 --use-singularity --singularity-args '\-e'
```
This will download the chip-eat container from docker hub and run the pipeline on the input peaks. 

**Note! One of the files in the examples is intended to NOT generate results!** When running the pipeline, the dataset named `EXP041075_JLP1586-Strain_fkh2` will not go through the entire processing. We provide this file in the examples to illustrate that the pipeline will not always generate TFBSs for a given ChIP-seq dataset. Typically, this is related to the quality of the ChIP-seq experiment. In this case, the `EXP041075_JLP1586-Strain_fkh2` dataset contains only one peak in the narrowPeak file. The pipeline creates a dummy file named `finished` within each dataset directory to flag successfully processed datasets. 

For each dataset, the resulting TFBSs in BED and fasta formats are found in the files named `DAMO/{matrix_ID}.{matrix_version}_rescan.score.ez.score_best.tsv.tfbs.bed` and `DAMO/{matrix_ID}.{matrix_version}_rescan.score.ez.score_best.tsv.tfbs.fa`, respectively.