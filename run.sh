cd /chip-eat/

CORES=$1
SLURM=$2

export PATH=/chip-eat/lib/usr/local/bin:$PATH
export PYTHONPATH=/chip-eat/lib/usr/local/lib/python3.8/site-packages/:/chip-eat/lib/usr/local/lib64/python3.8/site-packages/
export R_LIBS=/chip-eat/lib/usr/local/lib/R/library:$R_LIBS

if [ $SLURM == 0 ]
then
  python3.8 -m snakemake --cores $CORES -p
else
  python3.8 -m snakemake -s Snakefile_slurm --profile slurm -p
fi
