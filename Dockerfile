FROM fedora:35

# Create base directory
RUN mkdir /chip-eat

# Copy requirements for installation of dependencies
COPY ./requirements/ /chip-eat/requirements

# Copy necessary scripts and binaries
#COPY ./bin/ /chip-eat/bin
COPY ./src/ /chip-eat/src

WORKDIR /chip-eat

# Install system requirements
RUN sudo dnf -y install findutils.x86_64 \
    && xargs dnf -y install < requirements/requirements-sys.txt \
    && ln -s /usr/bin/python3.8 /usr/local/bin/python

# Install CUDA
RUN sudo dnf config-manager --add-repo https://developer.download.nvidia.com/compute/cuda/repos/fedora35/x86_64/cuda-fedora35.repo \
    && sudo dnf clean all \
    && sudo dnf -y module install nvidia-driver:latest-dkms \
    && sudo dnf -y install cuda

# Install bedtools
RUN mkdir bedtools \
    && git clone https://github.com/arq5x/bedtools2.git bedtools
WORKDIR /chip-eat/bedtools
RUN make \
    && make install
WORKDIR /chip-eat

# Get cuDAMO repository
RUN git clone https://github.com/oriolfornes/cuDAMO.git cuDAMO \
    && chmod +x cuDAMO/cudamo.py \
    && ln -s /chip-eat/cuDAMO/cudamo.py /usr/local/bin/cudamo

# Create non-root user to install rest of dependencies
RUN useradd -u 1001 user \
    && chown -R user /chip-eat
USER user

# Install python dependencies
RUN python3.8 -m ensurepip --root /chip-eat/lib
ENV PATH="/chip-eat/lib/usr/local/bin:${PATH}" 
ENV PYTHONPATH="/chip-eat/lib/usr/local/lib/python3.8/site-packages/:/chip-eat/lib/usr/local/lib64/python3.8/site-packages/"
RUN python3.8 -m pip install --root /chip-eat/lib -r requirements/requirements.txt 

# Install R dependencies
RUN mkdir -p /chip-eat/lib/usr/local/lib/R/library
ENV R_LIBS="/chip-eat/lib/usr/local/lib/R/library:${R_LIBS}"
RUN Rscript requirements/requirements.R
