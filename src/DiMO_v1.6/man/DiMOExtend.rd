\name{DiMOExtend}
\alias{DiMOExtend}
\title{DiMo Extend}
\description{
  Given a maximum length to extend, DiMOExtend extends the PFM by a same length sequencially on 5' and 3' end. DiMO extend outputs .csv file with the summary of training and validation AUCs when motif lengths are extended. This just motif extension program and doesnot do trimming. For Extension and Trimming see DiMOExtendAndTrim which. This function was mostly created for developers (my) use.
}
\usage{
DiMOExtend(positive.file,negative.file,pfm.file,output.flag,epochs=300)
}
\arguments{
  \item{positive.file}{File name containing sequences with motifs in FASTA format}
  \item{negative.file}{File name containing sequences with motifs in FASTA format}
  \item{pfm.file.name}{PFM file with preliminary motif
     The format goes here:  
        first line with name of TF starting with ">" 
        remaining four line should be PFM  
     
     > motif 1 \cr
     A | 0.000010 0.000010 0.661491 0.000010 0.000010  \cr
     C | 0.000010 0.000010 0.338509 0.000010 0.000010  \cr
     G | 0.999700 0.999700 0.000010 0.000010 0.000010  \cr
     T | 0.000010 0.000010 0.000010 0.999700 0.999700  \cr

     This motif will be exted sequencially.
  }
  \item{output.flag}{This flag is used to generate output file names (Default: check_output).  
               A summary of AUC on training and validation will be in output.flag"_valid_summary.csv"
              }

  \item{epochs}{Number of steps of optimization. (Default: 150). Higher the number of steps, higher the time program requires to run before convergence}
  \item{max.add}{Lengths to extend. (Default: 6)}
  \item{learning.rate}{Learning rates used during each epoch for generating next PWM. default is c(1,0.55,0.1).}
  \item{in.seed}{Seed that will be used to randomly split training and test set}
}
\seealso{
  \code{\link{DiMOExtendAndTrim}} 
  \code{\link{DiMO}} 
  \code{\link{computeJustAUC}} 
  \code{\link{onlyTrim}} 
}
\examples{
% Extend motif by 1 and 2 bases on either side of seed PFM and optimize extended motif on training set (80% of positive set) and validate using validation set (20% of positive set). The performance is tested with the same negative set.
% The AUCs are written in "output_TF_valid_summary.csv"

DiMOExtend(
    positive.file="TF_positive.fasta",
    negative.file="TF_negative.fasta",
    pfm.file.name="TF.pfm",
    output.flag="output_TF",
    epochs=30,
    max.add=6,
    learning.rates=seq(1,0.1,length.out=3),
    in.seed=100
    )
}
\keyword{file}
