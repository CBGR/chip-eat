\name{onlyTrim}
\alias{onlyTrim}
\title{Trim extended motif, mostly an output of DiMOExtend}
\description{
  Given an extend motif, onlyTrim trims the motif. First it cuts the motif to center four positions in the center and then keep adding one at 5' till AUC increases significantly. After that It adds one base at a time to 3' end till AUC increases significantly. To find shortest motif that gives the best AUC. 
  \cr The output of the program is trimmed motif in file "trimmed_`output.flag`.pfm"
  \cr This function is for developers use but you are free to use it. For other used DiMOExtendAndTrim is suggested. 
}
\usage{
onlyTrim(positive.file,negative.file,pfm.file,output.flag,in.seed)
}
\arguments{
  \item{positive.file}{File name containing sequences with motifs in FASTA format}
  \item{negative.file}{File name containing sequences with motifs in FASTA format}
  \item{pfm.file.name}{A long PFM, width greater than 18
     The format goes here:  \cr
        first line with name of TF starting with ">"  \cr
        remaining four line should be PFM   \cr
     
     > motif 1 \cr
     A | 0.000010 0.000010 0.661491 0.000010 0.000010  \cr
     C | 0.000010 0.000010 0.338509 0.000010 0.000010  \cr
     G | 0.999700 0.999700 0.000010 0.000010 0.000010  \cr
     T | 0.000010 0.000010 0.000010 0.999700 0.999700  \cr

     This motif will be exted sequencially.
  }
  \item{output.flag}{This flag is used to generate output file names (Default: check_output).  
               Trimmed motif in file "trimmed_`output.flag`.pfm"
              }
  \item{in.seed}{Seed that will be used to randomly split training and test set}
}
\seealso{
  \code{\link{DiMOExtendAndTrim}} 
  \code{\link{DiMO}} 
  \code{\link{computeJustAUC}} 
  \code{\link{onlyTrim}} 
}
\examples{

onlyTrim(
    positive.file="TF_positive.fasta",
    negative.file="TF_negative.fasta",
    pfm.file.name="TF.pfm",
    output.flag="output_TF",
    in.seed=100
    )
}
\keyword{file}
