\name{DiMO}
\alias{DiMO}
\title{Motifs Optimizer}
\description{
  Optimizes motifs given a preliminary PFM, a positive dataset (sequences with motif) and a negative dataset (sequences without motif). 
}
\usage{
%DiMO(positive.file,negative.file,pfm.file,output.flag,generations=30)
}
\arguments{
  \item{positive.file}{File name containing sequences with motifs in FASTA format}
  \item{negative.file}{File name containing sequences with motifs in FASTA format}
  \item{pfm.file.name}{PFM file with preliminary motif
     The format goes here:  
        first line with name of TF starting with ">" 
        remaining four line should be PFM  
     
     > motif 1 \cr
     A | 0.000010 0.000010 0.661491 0.000010 0.000010  \cr
     C | 0.000010 0.000010 0.338509 0.000010 0.000010  \cr
     G | 0.999700 0.999700 0.000010 0.000010 0.000010  \cr
     T | 0.000010 0.000010 0.000010 0.999700 0.999700  \cr
   }
  \item{output.flag}{This flag is used to generate output file name (Default: check_output).  
              Outputs are:
                 output.flag"_START.pfm" : starting pfm     
                 output.flag"_END.pfm"   : pfm resulted at the end of optimization
                 output.flag"_AUCS.txt"  : evolution of AUC as fuction of generations
                 output.flag"_plot.pdf"  : How the program has improved ROC: The red curve is ROC when program was started. The black curve is the ROC at the end of optimization. The other transparent curve shows show the ROC was during each step of optimization. The runs are colored in color space ranging from blue to light yellow. 
              }
  \item{epochs}{Number of steps of optimization. (Default: 30). Higher the number of steps, higher the time program requires to run}
  \item{add.at.five}{To extend PWM by that much positions at 5' end (Default: 0)}
  \item{add.at.three}{To extend PWM by that much positions at 3' end (Default: 0)}
  \item{learning.rate}{Learning rates used during each epoch for generating next PWM. default is c(1,0.55,0.1)}
}
\value{\item{auc}{AUC of optimized motif}}
\seealso{
  \code{\link{DiMOExtendAndTrim}}
  \code{\link{computeJustAUC}}
}
\examples{
% predict motif given a set of sequences containing motifs and not containing motifs
% look for bcd_1_positive.fasta and bcd_1_negative.fasta provided in the distribution
% and a pfm file
DiMO(
    positive.file="TF_positive.fasta",
    negative.file="TF_negative.fasta",
    pfm.file.name="TF.pfm",
    output.flag="output_TF",
    epochs=150,
    add.at.five=0,
    add.at.three=0,
    learning.rates=seq(1,0.1,length.out=3)
    )

}
\keyword{file}
