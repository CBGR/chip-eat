writeFasta <- function ( sequences, 
                         names.sequences, 
                         output.file) 

{

  if(length(sequences) != length(names.sequences)){
   error("Number of sequences and names dont match")
  }

  outfile <- file(description = output.file, 
                  open = "w")

  write.seq <- function(index){
    writeLines(paste(">",names.sequences[index]),outfile)
    writeLines(sequences[index],outfile)
  }

  lapply(1:length(sequences),write.seq)

  close(outfile)

}
