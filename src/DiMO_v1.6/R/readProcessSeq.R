###############################################################################
# function to process sequences This is again long function                   #
# kept so to maintain flow of program                                         #
# This program populates all key variables needed for iterations              #
###############################################################################

readProcessSeq <- function(positive.file,negative.file,motif.length){

  # environment to access key variables

  # read sequences in fasta format
  write("Reading Positive sequences",file="")
  positive.samples <- readFasta(positive.file)
  write("Reading Negative sequences",file="")
  negative.samples <- readFasta(negative.file)

  write("Start Processing sequences",file="")

  # store description
  positive.samples.desc <- positive.samples$description
  negative.samples.desc <- negative.samples$description

  # store sequences
  positive.samples <- list(V1=positive.samples$sequence)
  negative.samples <- list(V1=negative.samples$sequence)

  # check for illegal characters
  lapply(positive.samples$V1,checkSanity)
  lapply(negative.samples$V1,checkSanity)

  #return(positive.samples)

  # generate site lists
  pos.splitted <- splitAndGenerateList(in.array=positive.samples$V1,motif.length=motif.length)
  neg.splitted <- splitAndGenerateList(in.array=negative.samples$V1,motif.length=motif.length)

  # two lines added in v1.1 
  names(pos.splitted) <- paste("seq",sprintf("%.7d",1:length(pos.splitted)),sep="")
  names(neg.splitted) <- paste("seq",sprintf("%.7d",1:length(neg.splitted)),sep="")
  # -----------------------

  assign("pos.splitted", pos.splitted, envir=mopEnv)
  assign("neg.splitted", neg.splitted, envir=mopEnv)
  assign("motif.length",motif.length, envir=mopEnv)

  # unlist sites
  pos.splitted.unlist <- unlist(pos.splitted)
  neg.splitted.unlist <- unlist(neg.splitted)

  assign("pos.splitted.unlist",pos.splitted.unlist, envir=mopEnv)
  assign("neg.splitted.unlist",neg.splitted.unlist, envir=mopEnv)

  # converting unlist to list helper
  pos.seq.names.for.traceback <- rep(names(pos.splitted),lapply(pos.splitted,length))
  neg.seq.names.for.traceback <- rep(names(neg.splitted),lapply(neg.splitted,length))

  assign("pos.seq.names.for.traceback",pos.seq.names.for.traceback, envir=mopEnv)
  assign("neg.seq.names.for.traceback",neg.seq.names.for.traceback, envir=mopEnv)

  # how many positive and negative sequences are there
  number.of.positive.sequences <- length(pos.splitted)
  number.of.negative.sequences <- length(neg.splitted)

  assign("number.of.positive.sequences",number.of.positive.sequences, envir=mopEnv)
  assign("number.of.negative.sequences",number.of.negative.sequences, envir=mopEnv)

  # variable to store auc assigned in mopEnv space
  assign("global.auc",NULL,envir=mopEnv)

  write("Done Processing sequences",file="")

}
