DIR=$( realpath "$( dirname "$0" )" )

# ===== #
# Usage #
# ===== #

usage() {
    echo "Usage: $0 -d DATA DIR -p PEAKS DIR -c CORES"
      echo ""
      echo "Run chip-eat pipeline with given data and peaks directories."
      echo ""
      echo "Arguments:"
      echo ""
      echo "    -d DATA DIR: path to a directory with the necessary data (see the \"Data directory preparation\" section in the documentation)."
      echo "    -p PEAKS DIR: path to a directory with the narrowPeak files (see the \"Peaks directory preparation\" section in the documentation)."
      echo "    -c CORES: number of cores to run in parallel (default: 2)."
      echo "    -m METHOD: container method to use (valid values are docker or singularity)."
      echo ""
      exit 0
}

# ================ #
# Argument parsing #
# ================ #

echo "Parsing arguments..."

cores=2
slurm=0

while getopts "d:p:c:m:sh" options; do
  case "${options}" in
    d) data_dir=${OPTARG} ;;
    p) peaks_dir=${OPTARG};;
    c) cores=${OPTARG} ;;
    m) method=${OPTARG} ;;
    s) slurm=1 ;;
    h) usage ;;
  esac
done

# ====================================== #
# Exit if required arguments are not met #
# ====================================== #

if [ -z $data_dir ]
then
  echo "No data directory provided. Exiting..."
  usage
elif [ -z $peaks_dir ]
then
  echo "No peaks directory provided. Exiting..."
  usage
elif [ -z $method ]
then
  echo "No container method provided. Exiting..."
  usage
fi

# ============ #
# Run pipeline #
# ============ #

echo "Running chip-eat with the following parameters:"
  echo "    - Data directory: $(realpath ${data_dir})"
  echo "    - Peaks directory: $(realpath ${peaks_dir})"
  echo "    - Number of cores: ${cores}"
  echo "    - Container method: ${method}"
  echo "    - Slurm: ${slurm}"

if [ $method == "docker" ]
then

  # Docker

  data_dir_vol="$(realpath ${data_dir}):/chip-eat/data/:rw"
  peaks_dir_vol="$(realpath ${peaks_dir}):/chip-eat/results/:rw"

  docker run \
    --env CORES=$cores \
    --env SLURM=$slurm \
    -v $data_dir_vol \
    -v $peaks_dir_vol \
    -t firriver/chip-eat:1.0

elif [ $method == "singularity" ]
then

  # Singularity
  mkdir -p ${peaks_dir}/.snakemake
  bind_paths="$(realpath ${data_dir}):/chip-eat/data,$(realpath ${peaks_dir}):/chip-eat/results,$(realpath ${peaks_dir})/.snakemake:/chip-eat/.snakemake"

  singularity run \
    --env CORES=$cores \
    --env SLURM=$slurm \
    --bind $bind_paths \
    docker://firriver/chip-eat:1.0

fi